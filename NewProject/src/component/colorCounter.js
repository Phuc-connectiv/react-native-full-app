import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, Button } from 'react-native';

const ColorCounter = ({color, onIncrease, onDecrease}) => {
    return(
        <View>
            <Button title={`Increase ${color}`} onPress={() => onIncrease()}/>
            <Button title={`Decrease ${color}`} onPress={() => onDecrease()}/>
        </View>
    );
}
export default ColorCounter;