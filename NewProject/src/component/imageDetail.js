import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';

const ImageDetail = (props) => {
    // const {name} = props;
    return(
        <View>
            <Text>Image {props.name}</Text>
            <View >
                <Image 
                    style={styles.image} 
                    source={props.imageSource}
                />
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
      fontSize: 30
    },
    image: {
        width: 150,
        height: 150
    }
});
export default ImageDetail;