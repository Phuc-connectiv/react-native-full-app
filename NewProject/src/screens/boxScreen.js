import React, { useState } from 'react';
import { View, Text, StyleSheet,TextInput } from 'react-native';

const BoxScreen = () => {
    return (
        <View style={styles.viewStyle}>
            <Text style={styles.viewText}>Box Screen</Text>
            <Text style={styles.viewText}>Box Screen</Text>
            <Text style={styles.viewText}>Box Screen</Text>
        </View>
    );
}
const styles = StyleSheet.create({
    viewStyle: {
        borderWidth: 5,
        borderColor: 'black'
    },
    viewText: {
        borderWidth: 5,
        borderColor: 'red',
    }
});
export default BoxScreen;