import React from 'react';
import { StyleSheet, Text, View, Button,ScrollView } from 'react-native';

const HomeScreen = (props) => {
    const hello = 'welcome to Home';
  return (
    <View style={styles.container}>
        <ScrollView>
        <Text style={styles.text}>Home screen</Text>
        <Text>{hello}</Text>
        <Button 
            title="Go to Component"
            onPress={() => props.navigation.navigate('Components')}
        />
        <Button 
            title="Go to Lists"
            onPress={() => props.navigation.navigate('Lists')}
        />
        <Button 
            title="Go to Image"
            onPress={() => props.navigation.navigate('Image')}
        />
        <Button 
            title="Go to Counter"
            onPress={() => props.navigation.navigate('Counter')}
        />
        <Button 
            title="Go to Counter2"
            onPress={() => props.navigation.navigate('Counter2')}
        />
        <Button 
            title="Go to Color"
            onPress={() => props.navigation.navigate('Color')}
        />
         <Button 
            title="Go to Square"
            onPress={() => props.navigation.navigate('Square')}
        />
         <Button 
            title="Go to Text"
            onPress={() => props.navigation.navigate('Text')}
        />
        <Button 
            title="Go to Box"
            onPress={() => props.navigation.navigate('Box')}
        />
        </ScrollView>
    </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
      fontSize: 30
  }
});
export default HomeScreen;