import React, { useState } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button
} from 'react-native';

const CounterScreen2 = () => {
    const [counter, setCounter] = useState(0);
    return (
        <View style={styles.container}>
            <Text>Current Counter: {counter}</Text>
            <View style={styles.but}>
                <Button
                    title="Increase"
                    onPress={()=> {
                        setCounter(counter + 1)
                    }}
                />
                <Button
                    title="Decrease"
                    onPress={()=> {
                        setCounter(counter - 1)
                    }}
                />
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    but: {
        flexDirection: 'row-reverse'
    }
});
export default CounterScreen2;