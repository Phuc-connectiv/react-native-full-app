import React, { useState } from 'react';
import { View, Text, Style, StyleSheet } from 'react-native';
import ColorCounter from '../component/colorCounter';

 const SquareScreen = () => {
    const [ red, SetRed ]  = useState(0);
    const [ green, SetGreen ]  = useState(0);
    const [ blue, SetBlue ]  = useState(0);
    const point = 20;
    const setColor = (color, change) => {
        if(color === red){
            if(red + change > 255 || red + change < 0){
                return ;
            }else{
                SetRed(red + change);
            }
        }
        switch(color){
            case red:
                red + change > 255 || red + change < 0 ? null : SetRed(red + change);
                return;
            case green:
                green + change > 255 || green + change < 0 ? null : SetGreen(green + change);
                return;
            case blue:
                blue + change > 255 || blue + change < 0 ? null : SetBlue(blue + change);
                return;
        }
    }
    return(
        <View style={styles.container}>
            <Text>Square Screen</Text>
            <ColorCounter 
                color="Red"
                onIncrease={() => {
                    setColor(red, point)
                }}
                onDecrease={() => {
                    setColor(red, -1 * point)
                }}
            />
            <ColorCounter 
                color="Green"
                onIncrease={() => SetGreen(green + point)}
                onDecrease={() => SetGreen(green - point)}
            />
            <ColorCounter 
                color="Blue"
                onIncrease={() => SetBlue(blue + point)}
                onDecrease={() => SetBlue(blue - point)}
            />
            <View style={{width: 100, height: 100, backgroundColor: `rgb(${red}, ${green}, ${blue})` }} />
        </View>
    );
 }
 const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      },
      text: {
          fontSize: 30
      }
 });
 export default SquareScreen;