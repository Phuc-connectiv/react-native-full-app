import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const ComponentScreen = () => {
    const hello = 'welcome to Component'
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Component Screen</Text>
      <Text>{hello}</Text>
    </View>
    );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 30
}
});
export default ComponentScreen;