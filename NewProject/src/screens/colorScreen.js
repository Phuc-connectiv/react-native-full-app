import React, { useState } from 'react';
import {
    View,
    Text,
    Button,
    StyleSheet,
    FlatList
} from 'react-native';

const ColorScreen = () => {
    const [ color, setColor ] = useState([]);
    return (
        <View style={styles.container}>
            <View style={{width: 100, height: 100, backgroundColor: ranDomRbg() }} />
            <Button 
                title="Add a color"
                onPress={() => {
                    setColor([...color, ranDomRbg()])
                }}
            />
             <FlatList
                horizontal
                data={color}
                renderItem={({item}) => 
                    <View style={{width: 100, height: 100, backgroundColor: ranDomRbg() }} />
                }
                keyExtractor={item => item }
            />
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
        fontSize: 30
    }
  });
const ranDomRbg = () => {
    const red = Math.floor(Math.random() * 256);
    const green = Math.floor(Math.random() * 256);
    const blue = Math.floor(Math.random() * 256);
    return `rgb(${red}, ${green}, ${blue})`;
};
export default ColorScreen;