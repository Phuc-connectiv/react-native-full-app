import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import ImageDetail from '../component/imageDetail';
import Image_1 from '../../assets/image/1.jpg';

const ImageScreen = () => {
    const hello = 'welcome to Image'
  return (
        <ScrollView>
            <View style={styles.container}>
                <Text style={styles.text}>Image Screen</Text>
                <Text>{hello}</Text>
                <ImageDetail name='Forest' imageSource={Image_1}/>
                <ImageDetail name='Mountain' imageSource={Image_1}/>
                <ImageDetail name='Beach' imageSource={Image_1}/>
            </View>
        </ScrollView>
    );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 30
}
});
export default ImageScreen;