import React, { useState } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

const CounterScreen = () => {
    const hello = 'welcome to Counter';
    const [count, setCount] = useState(0);
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Counter Screen</Text>
      <Text>{hello}</Text>
      <Text>Counter Current: {count}</Text>
      <View style={styles.but}>
        <Button
            title="Increase"
            onPress={() => {
                setCount(count + 1)
            }}
        />
        <Button
            title="Decrease"
            onPress={() => {
                setCount(count - 1)
            }}
        />
      </View>
    </View>
    );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 30
  },
  but: {
      marginTop: 30,
      flexDirection: 'row-reverse'
  }
});
export default CounterScreen;