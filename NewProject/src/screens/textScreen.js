import React, { useState } from 'react';
import { View, Text, StyleSheet,TextInput } from 'react-native';

const TextScreen = () => {
    state = {age: ''};
    const [ name, setName ] = useState('phuc');
    const [ age, setAge ] = useState('99');
    return (
        <View style={styles.container}>
            <Text>Name: </Text>
            <TextInput 
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={ false }
                value={ name }
                onChangeText={ newValue => setName(newValue) }
            />
            <Text>My name is { name }</Text>
            <Text>Age: </Text>
            <TextInput 
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={ false }
                value={ age }
                onChangeText={ newValue => setAge(newValue) }
            />
            <Text>My name is { age }</Text>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
      },
      input: {
          width: '90%',
          height: 40,
          margin: 15,
          borderColor: 'red',
          borderWidth: 1,
          padding : 10,
      }
});
export default TextScreen;