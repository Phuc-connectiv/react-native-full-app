import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';

const ListScreen = () => {
    const hello = 'welcome to List';
    const frends = [
        {name: 'A'},{name: 'B'},{name: 'C'},{name: 'D'},{name: 'E'},{name: 'F'},{name: 'G'},{name: 'H'},{name: 'I'},{name: 'J'},
        {name: 'K'},{name: 'L'},{name: 'M'},{name: 'N'},{name: 'O'},{name: 'P'},{name: 'Q'},{name: 'S'}
    ]
    return (
        <View>
            <Text style={styles.text}>Lists Screen</Text>
            <Text>{hello}</Text>
            <FlatList
                data={frends}
                renderItem={({item}) => {
                    return <Text>{ item .name }</Text>
                }}
                keyExtractor={(item) => item.name }
                horizontal
                showsHorizontalScrollIndicator={false}
            />
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
      fontSize: 30
  }
});
export default ListScreen;