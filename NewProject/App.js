import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import HomeScreen from './src/screens/HomeScreen';
import ComponentScreen from './src/screens/componentScreen';
import ListScreen from './src/screens/list';
import ImageScreen from './src/screens/imageScreen';
import CounterScreen from './src/screens/counter';
import CounterScreen2 from './src/screens/counter2';
import ColorScreen from './src/screens/colorScreen';
import SquareScreen from './src/screens/squareScreen';
import TextScreen from './src/screens/textScreen';
import BoxScreen from './src/screens/boxScreen';

const navigator = createStackNavigator(
  {
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      title: 'Home'
    }
  },
  Components: {
    screen: ComponentScreen,
    navigationOptions: {
      title: 'Component'
    }
  },
  Lists: {
    screen: ListScreen,
    navigationOptions: {
      title: 'List'
    }
  },
  Image: {
    screen: ImageScreen,
    navigationOptions: {
      title: 'Image'
    }
  },
  Counter: {
    screen: CounterScreen,
    navigationOptions: {
      title: 'Counter'
    }
  },
  Counter2: {
    screen: CounterScreen2,
    navigationOptions: {
      title: 'Counter 2'
    }
  },
  Color: {
    screen: ColorScreen,
    navigationOptions: {
      title: 'Color'
    }
  },
  Square: {
    screen: SquareScreen,
    navigationOptions: {
      title: 'Square'
    }
  },
  Text: {
    screen: TextScreen,
    navigationOptions: {
      title: 'Text'
    }
  },
  Box: {
    screen: BoxScreen,
    navigationOptions: {
      title: 'Box'
    }
  }
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      title: 'App'
    }
  }
);
export default createAppContainer(navigator);
