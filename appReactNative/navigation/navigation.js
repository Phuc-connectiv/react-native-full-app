import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import {
  Header,
  Left,
  Right,
  Body,
  Button,
  Title,
  Icon
} from 'native-base';
import HomeScreen from '../screens/homeScreen';
import CountScreen from '../screens/counterScreen';

const navigationHandler = (navigator) => ({Header: null});
const HomeStack = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: navigationHandler
  },
  CountScreen: {
    screen: CountScreen,
    navigationOptions: navigationHandler
  }
});
export default HomeStack;
