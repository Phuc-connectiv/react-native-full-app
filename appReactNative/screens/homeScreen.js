import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Button
} from 'react-native';

const HomeScreen = () => {
    return (
        <View style={styles.container}>
            <Button 
                title="Go to count" 
            />
            <Button 
                title="Go to color" 
            />
        </View>
    );
} 
export default HomeScreen;
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });