import React from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import HomeStack from './navigation/navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import HomeScreen from './screens/homeScreen';
// const Home = createAppContainer(HomeStack);
export default class App extends React.Component {
  render() {
    return (
      <View>
          <HomeScreen/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
